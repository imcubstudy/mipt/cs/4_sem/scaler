#ifndef __linux__
    #error "This is linux-specific code, macro __linux__ not detected"
#endif
#define _GNU_SOURCE
#pragma GCC optimize ("02")

#ifndef CPUINFO
    #error "CPUINFO not defined"
#endif
#define MAXTHPCPU       128
#define MAXCORES        64
#define MAXCPUPCORE     2
#define HTK             1.6
struct {
    int ncores;
    int ncpu;
    int ntpc;
    int cpu[MAXCORES][MAXCPUPCORE];
} cpuinfo;
void parseCPUINFO(void) __attribute__((constructor()));

#define _str(x) #x
#define str(x) _str(x)
#define $(code...)                                                  \
    do {                                                            \
        errno = 0;                                                  \
        code;                                                       \
        if(errno != 0) {                                            \
            perror(#code " at " str(__LINE__));                     \
            exit(EXIT_FAILURE);                                     \
        }                                                           \
    } while(0)

#define TEST(cond, msg)                                             \
    do {                                                            \
        if(!(cond)) {                                               \
            fprintf(stderr, msg "\n");                              \
            fflush(stderr);                                         \
            exit(EXIT_FAILURE);                                     \
        }                                                           \
    } while(0)

#define _cmp(op, x, y)                                              \
    ({                                                              \
        typeof(x) __x = (x);                                        \
        typeof(y) __y = (y);                                        \
        __x op __y ? __x : __y;                                     \
    })
#define min(x, y) _cmp(<, x, y)
#define max(x, y) _cmp(>, x, y)

#include <sched.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <float.h>
#include <unistd.h>
#include <string.h>

#define LOWB    -400
#define HIGHB   400
#define LGTH    ((double)(HIGHB - LOWB))
#define dX      4e-8
#define FUNC(x) (0.25 * (x)*(x) - (x))

typedef struct {
    double      lb;
    double      hb;
    double      res;
} targs_t;

typedef struct {
    targs_t         args;
    pthread_t       tid;
} tinfo_t;

typedef struct {
    struct {
        tinfo_t th[MAXTHPCPU];
        int     n;
        int     comp;
    } cpu[MAXTHPCPU];
} core_threadset_t;

double distribute(core_threadset_t *cts, int amount);

void *compute(void *arg);

int main(int argc, char **argv)
{
    int t, c, i;
    TEST(argc == 2, "Wrong amount of arguments");
    
    int nthreads = atoi(argv[1]);
    TEST(nthreads <= MAXTHPCPU * cpuinfo.ncpu, "That's too much, maaan!");

    core_threadset_t cts[cpuinfo.ncores];
    memset(cts, 0, sizeof(cts));

    printf("eff %lf%%\n", distribute(cts, nthreads) * 100);

    cpu_set_t       set;
    pthread_attr_t  pta;
    $(pthread_attr_init(&pta));
    for(t = 0; t < cpuinfo.ntpc; ++t) {
        for(c = 0; c < cpuinfo.ncores; ++c) {
            CPU_ZERO(&set);
            CPU_SET(cpuinfo.cpu[c][t], &set);
            $(pthread_attr_setaffinity_np(&pta, sizeof(set), &set));

            for(i = 0; i < cts[c].cpu[t].n; ++i) {
                tinfo_t *th = &cts[c].cpu[t].th[i];
                $(pthread_create(&(th->tid), &pta, compute, &(th->args)));
            }
        }
    }
    pthread_attr_destroy(&pta);

    double res = 0;
    for(t = 0; t < cpuinfo.ntpc; ++t) {
        for(c = 0; c < cpuinfo.ncores; ++c) {
            for(i = 0; i < cts[c].cpu[t].comp; ++i) {
                $(pthread_join(cts[c].cpu[t].th[i].tid, NULL));
                res += cts[c].cpu[t].th[i].args.res;
            }
        }
    }

    printf("res = %lf\n", res);

    return 0;
}

double distribute(core_threadset_t *cts, int amount)
{
    int t, c, i;
    int stop = 0;
    int resolved = 0;
    for(t = 0; !stop; t = (t + 1) % cpuinfo.ntpc) {
        for(c = 0; !stop && c < cpuinfo.ncores; c++) {
            cts[c].cpu[t].n++;
            cts[c].cpu[t].comp += (resolved < amount) ? 1 : 0;

            resolved++;
            if(resolved >= amount && resolved >= cpuinfo.ncores)
                stop = 1;
        }
    }
    
    int coreload[cpuinfo.ncores];
    int nhalf = 0, nfull = 0;
    for(c = 0; c < cpuinfo.ncores; ++c) {
        int rate = 0;
        for(t = 0; t < cpuinfo.ntpc; ++t)
            rate += (cts[c].cpu[t].comp > 0) ? 1 : 0;
        if(rate == 1) {
            nhalf += rate;
        } else if(rate == 2) {
            nfull += rate;
        }
        coreload[c] = rate;
    }
    if(cpuinfo.ntpc == 1) {
        nfull = nhalf;
        nhalf = 0;
    }

    double LF = LGTH / (nhalf * HTK + nfull); 
    double LH = LF * HTK;
    if(cpuinfo.ntpc == 1) {
        LH = LF;
    }

    double lb = LOWB;
    for(c = 0; c < cpuinfo.ncores; ++c) {
        double Lcore = (coreload[c] == 1) ? LH : 2 * LF;
        
        double Lcpu = Lcore / coreload[c];
        for(t = 0; t < cpuinfo.ntpc; ++t) {
               
            double delt = Lcpu / cts[c].cpu[t].comp;
            for(i = 0; i < cts[c].cpu[t].comp; ++i) {
                targs_t *args = &cts[c].cpu[t].th[i].args;

                args->lb = lb;
                args->hb = lb + delt;
                lb += delt;
            }
            for(i = cts[c].cpu[t].comp; i < cts[c].cpu[t].n; ++i) {
                targs_t *args = &cts[c].cpu[t].th[i].args;

                args->lb = LOWB;
                args->hb = HIGHB;
            }
        }
    }

    return (nhalf * HTK + nfull) / cpuinfo.ncpu;
}

void *compute(void *arg)
{
    targs_t *targs = (targs_t*)arg;
    double lb = targs->lb;
    double hb = targs->hb;

    double x;
    double res = 0;
    for(x = lb; x < hb; x += dX) {
        res += FUNC(x);
    }
    targs->res = res * dX;

    pthread_exit(0);
}

void parseCPUINFO()
{
    char info[] = str(CPUINFO);
    int sh = 0, delt = -1;
    int core, cpu;
    while(({sscanf(info + sh, "%d%d%n", &core, &cpu, &delt);
        sh += delt;
        *(info + sh - delt);
    })) {
        cpuinfo.ncores = max(core + 1, cpuinfo.ncores);
        cpuinfo.ncpu++;
    }
    cpuinfo.ntpc = cpuinfo.ncpu / cpuinfo.ncores;

    int init[cpuinfo.ncores];
    memset(init, 0, sizeof(int) * cpuinfo.ncores);

    sh = 0;
    while(({sscanf(info + sh, "%d%d%n", &core, &cpu, &delt);
        sh += delt;
        *(info + sh - delt);
    })) {
        cpuinfo.cpu[core][init[core]] = cpu;
        init[core]++;
    }
}
