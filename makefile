CC := gcc
CPUINFO := $(shell 	lscpu -p=Core,CPU | 										\
					tr ',' ' ' | 												\
					egrep "[0-9]+ [0-9]+")

CFLAGS := -pthread -g -DCPUINFO="$(CPUINFO)"

scaler: scaler.c
	@$(CC) -o $@ $^ $(CFLAGS)
	@echo "Done."

test: scaler
	@for number in $(shell seq 16); do              							\
        res=$$(time 2>/dev/stdout -f "%e %P" ./scaler $$number 1>/dev/null) ;   \
        echo "nthreads = $$number tm = $$res" ;                                 \
    done

clean:
	rm scaler

